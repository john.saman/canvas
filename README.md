# Canvas Excercise

## Installation
```shell
git clone https://gitlab.com/john.saman/canvas.git path/to/dir
```

## Open with browser
```
/path/to/dir/canvas.html
```