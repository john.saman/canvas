circles = [];　//円の位置保持する
dragging = false; //移動中の時に真になる
activeCircle = null; // 選択された円
dragFirstPoint = null; //円移動の元の点


// 行為履歴を保持する
class ActionsHistory {
  constructor () {
    this.actionsHistory = [];
    this.currentCursor = 0;
    this.historySize = 0;
  }

  addCreate(circle, x, y) {
    this.actionsHistory[this.currentCursor++] = {
      action: 'create',
      circle: circles[circles.length-1],//円本体保持するため
      index: circles.length-1,          //円配列の中の位置保持するため
      xAxis: x,　                //表示位置を保持
      yAxis: y
    };
    this.historySize = this.currentCursor;
  }
  addMove(circle, x, y) {
    this.actionsHistory[this.currentCursor++] = {
      action: 'move',
      circle: circle,
      xAxis1: x,
      yAxis1: y,
    };
    this.historySize = this.currentCursor;
  }
  continueMove(activeCircle) {
    if (this.actionsHistory[this.currentCursor-1]['action'] == 'move') {
      if (this.actionsHistory[this.currentCursor-1]['xAxis1'] == activeCircle['x'] &&
      this.actionsHistory[this.currentCursor-1]['yAxis1'] == activeCircle['y']) {
        // 移動ではないから、歴史の項目の削除
        this.currentCursor --
        this.historySize --
      } else {
        this.actionsHistory[this.currentCursor-1]['xAxis2'] = activeCircle['x']
        this.actionsHistory[this.currentCursor-1]['yAxis2'] = activeCircle['y']
      }
    }
  }
  addDelete(circle, index) {
    this.actionsHistory[this.currentCursor++] = {
      action: 'delete',
      circle: circle,
      index: index
    };
    this.historySize = this.currentCursor;
  }
  undo(circles) {
    switch(this.actionsHistory[this.currentCursor-1].action) {
      case 'create':
      {
        deleteCircle(this.actionsHistory[this.currentCursor-1].circle)
        break;
      }
      case 'move':
      {
        this.actionsHistory[this.currentCursor-1].circle.x = this.actionsHistory[this.currentCursor-1].xAxis1
        this.actionsHistory[this.currentCursor-1].circle.y = this.actionsHistory[this.currentCursor-1].yAxis1
        break;
      }
      case 'delete':
      {
        circles.splice(this.actionsHistory[this.currentCursor-1].index, 0, this.actionsHistory[this.currentCursor-1].circle);

        break;
      }
    }
    this.currentCursor--;
  }
  redo() {
    switch(this.actionsHistory[this.currentCursor].action) {
      case 'create':
      {
        circles.splice(this.actionsHistory[this.currentCursor].index, 0, this.actionsHistory[this.currentCursor].circle);
        break;
      }
      case 'move':
      {
        //円の位置を円の移動先で設定する
        this.actionsHistory[this.currentCursor].circle.x = this.actionsHistory[this.currentCursor].xAxis2
        this.actionsHistory[this.currentCursor].circle.y = this.actionsHistory[this.currentCursor].yAxis2
        break;
      }
      case 'delete':
      {
        deleteCircle(this.actionsHistory[this.currentCursor].circle);

        break;
      }
    }
    this.currentCursor++;
  }
}

let actionsHistory = new ActionsHistory()
$(document).ready(function() {
  //履歴ボタンの状況の初期化
  updateUndo();
  updateRedo();

  $('#circles').on ('mouseup', function(e) {
    e.preventDefault()

    if ($('#creation').prop('checked')) {
      createCircle(e.offsetX, e.offsetY);

      actionsHistory.addCreate(circles[circles.length-1], e.offsetX, e.offsetY)
    } else {
      if (dragging) {
        actionsHistory.continueMove(activeCircle);
      }
    }

    updateUndo();
    updateRedo();

    dragging = false;
  });

  $('#circles').on ('mousemove', function(e) {
    e.preventDefault();

    // 円の移動
    if (dragging) {

      // 円を移動する
      moveCircle (activeCircle,
        e.offsetX-dragFirstPoint.x, e.offsetY-dragFirstPoint.y)


      // ドラーグ開始点を保持する
      dragFirstPoint.x = e.offsetX
      dragFirstPoint.y = e.offsetY
    }
  });

  $('#circles').on ('mousedown', function(e) {
    e.preventDefault()

    if (!$('#creation').prop('checked')) {
      activeCircle = null

      for (circle of circles) {
        if (isIntersecting(circle, 30, {
          x: e.offsetX,
          y: e.offsetY
        })) {
          // ドラッグ対象の円設定する
          activeCircle = circle

          //ドラッグの元のポイント設定
          dragFirstPoint = {
            x: e.offsetX,
            y: e.offsetY
          }

          //ドラーグモードのフラッグ設定
          dragging = true

          actionsHistory.addMove (activeCircle, activeCircle.x, activeCircle.y)
        }
      }
    }
    redraw();
  });

  $('#delete').on ('click', function(e) {
    i = deleteCircle(activeCircle)

    actionsHistory.addDelete (activeCircle, i)
  });

  $('#undo').on ('click', function(e) {
    actionsHistory.undo(circles)

    updateUndo();
    updateRedo();

    redraw();
  });

  $('#redo').on ('click', function(e) {
    actionsHistory.redo(circles)

    updateUndo();
    updateRedo();

    redraw();
  });
});

function createCircle(x, y) {
  // push the center of the new circle into circles stack
  circles.push(
    {
      x: x,
      y: y
    }
  );

  redraw();
}

function moveCircle(circle, deltaX, deltaY) {
  circle.x += deltaX
  circle.y += deltaY

  // 円の移動の後に
  redraw()
}

function deleteCircle(circleToDelete) {
  i = 0;
  for (circle of circles) {
    if (circle == circleToDelete) {
      circles.splice(i, 1);

      redraw();
      return i;
    }
    i++;
  }
}
function drawCircle(ctx, x, y, radius) {

  // 円の作成
  ctx.beginPath();
  ctx.arc(x, y, radius, 0, 2 * Math.PI);
  ctx.stroke();

  //円の中に色つける
  ctx.fill();
}

/*
 * circlesのデータを使ってcanvasに円を描く
*/
function redraw() {
  var c = document.getElementById("circles");
  var ctx = c.getContext("2d");

  ctx.lineWidth = 2
  ctx.strokeStyle = "#FF0000"
  ctx.fillStyle = "#FF00FF"

  //canvas上の円を全て削除
  ctx.clearRect(0, 0, 600, 400)

  //canvas上の円を再描画
  var reversed = circles.reverse()

  for (circle of reversed) {
    drawCircle(ctx, circle.x, circle.y, 30)

    if (activeCircle == circle) {
      //矩形描く
      ctx.strokeStyle = "#9DCCE0"

      ctx.rect(circle.x-30, circle.y-30, 60, 60);
      ctx.stroke();
    }
  }
  //元の配列に戻す
  circles.reverse()
}

/*クリックしたポイントが円とIntersectionしているかどうかtrue/falseの値を返却する*/
function isIntersecting(centerPoint, radius, clickPoint) {
  var distance = ((clickPoint.x - centerPoint.x) ** 2 + (clickPoint.y - centerPoint.y) ** 2) ** 0.5
  return distance <= radius
}

function updateUndo () {
  if (actionsHistory.currentCursor <= 0) {
    $('#undo').prop('disabled', true)
  } else {
    $('#undo').removeAttr('disabled')
  }
}

function updateRedo () {
  if (actionsHistory.currentCursor >= actionsHistory.historySize) {
    $('#redo').prop('disabled', true);
  } else {
    $('#redo').removeAttr('disabled');
  }
}
